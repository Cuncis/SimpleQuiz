package com.reksa.karang.simplequiz;

public class QuestionLibrary {
    private String mQuestions[] = {
            "Jaringan Komputer Lokal yaitu...",
            "Pendiri microsoft adalah...",
            "Ibu Kota Indonesia yaitu ...",
            "Ketuhanan yang Maha Esa adalah sila ke ...",
            "Indonesia berada di benua ..."
    };

    private String mChoices[][] = {
            {"LAN ", "MAN", "WAN"},
            {"Bill Gates", "Larry Page", "Steve Jobs"},
            {"Bandung", "Jakarta", "Surabaya"},
            {"Sila ke 4", "Sila ke 5", "Sila ke 1"},
            {"Amerika", "Eropa", "Asia"}
    };

    private String mCorrectAnswers[] = {
            "LAN", "Bill Gates", "Jakarta", "Sila ke 1", "Asia"
    };

    public String getQuestions(int a) {
        String question = mQuestions[a];
        return question;
    }

    public String getChoice1(int a) {
        String choice0 = mChoices[a][0];
        return choice0;
    }

    public String getChoice2(int a) {
        String choice1 = mChoices[a][1];
        return choice1;
    }

    public String getChoice3(int a) {
        String choice2 = mChoices[a][2];
        return choice2;
    }

    public String getCorrectAnswer(int a) {
        String answer = mCorrectAnswers[a];
        return answer;
    }

    public int getLength() {
        return mQuestions.length;
    }
}
