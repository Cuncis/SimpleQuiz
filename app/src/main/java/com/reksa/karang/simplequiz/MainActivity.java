package com.reksa.karang.simplequiz;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private QuestionLibrary mQuestionLibrary = new QuestionLibrary();

    private TextView tvScoreView, tvQuestionView;
    private Button btnChoice1, btnChoice2, btnChoice3;

    private String mAnswer;
    private int mScore = 0;
    private int mQuestionNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvScoreView = findViewById(R.id.tv_score);
        tvQuestionView = findViewById(R.id.tv_question);
        btnChoice1 = findViewById(R.id.btn_choice1);
        btnChoice2 = findViewById(R.id.btn_choice2);
        btnChoice3 = findViewById(R.id.btn_choice3);

        updateQuestion();

        btnChoice1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnChoice1.getText().equals(mAnswer)) {
                    mScore = mScore + 1;
                    updateScore(mScore);
                    updateQuestion();
                    Toast.makeText(MainActivity.this, "TRUE", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "FALSE", Toast.LENGTH_SHORT).show();
                    updateQuestion();
                }
            }
        });

        btnChoice2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnChoice2.getText().equals(mAnswer)) {
                    mScore = mScore + 1;
                    updateScore(mScore);
                    updateQuestion();
                    Toast.makeText(MainActivity.this, "TRUE", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "FALSE", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnChoice3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnChoice3.getText().equals(mAnswer)) {
                    mScore = mScore + 1;
                    updateScore(mScore);
                    updateQuestion();
                    Toast.makeText(MainActivity.this, "TRUE", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "FALSE", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void updateScore(int point) {
        tvScoreView.setText(""+mScore);
    }

    private void gameOver() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your score is " + mScore + " point")
                .setCancelable(false)
                .setPositiveButton("Play Again ",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            }
                        })
                .setNegativeButton("Exit",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void updateQuestion() {
        if (mQuestionNumber < mQuestionLibrary.getLength()) {
            tvQuestionView.setText(mQuestionLibrary.getQuestions(mQuestionNumber));
            btnChoice1.setText(mQuestionLibrary.getChoice1(mQuestionNumber));
            btnChoice2.setText(mQuestionLibrary.getChoice2(mQuestionNumber));
            btnChoice3.setText(mQuestionLibrary.getChoice3(mQuestionNumber));

            mAnswer = mQuestionLibrary.getCorrectAnswer(mQuestionNumber);
            mQuestionNumber++;
        } else {
            gameOver();
        }
    }
}
